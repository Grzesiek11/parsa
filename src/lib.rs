use std::{collections::HashMap, mem};

pub fn parse_arguments<'a, S>(command_tree: &'a CommandTree, arguments: &[S]) -> Result<ParsedCommand<'a>, Error>
    where S: AsRef<str>
{
    enum Context<'a> {
        Default,
        OptionValue(&'a str),
    }

    let mut context = Context::Default;
    let mut parsed_command = ParsedCommand {
        command: Vec::new(),
        arguments: HashMap::new(),
    };
    let mut command: Option<&Command> = None;
    let mut current_position: u8 = 0;

    for argument in arguments {
        let argument = argument.as_ref();
        let command_ensured = command.map_or(command_tree, |c| &c.commands);
        // TODO: Use [`Borrow`](https://github.com/sunshowers-code/borrow-complex-key-example) to do this better
        if let Some((command_id, new_command)) = command_ensured.iter()
            .find(|(id, _)| **id == CommandIdRef::Name(argument))
            .or_else(|| command_ensured.get_key_value(&CommandId::Default))
        {
            parsed_command.command.push(command_id.as_ref());
            command = Some(new_command);
        } else {
            let command = command.ok_or(Error::UnknownCommand)?;
            let current_context = mem::replace(&mut context, Context::Default);
            match current_context {
                Context::Default => {
                    if let Some(mut long_option) = argument.strip_prefix("--") {
                        let value = if let Some((option, value)) = long_option.split_once('=') {
                            long_option = option;
                            Some(value)
                        } else {
                            None
                        };

                        let mut found = false;
                        for (argument_id, command_argument) in &command.arguments {
                            if let ArgumentKind::Option { long, .. } = &command_argument.kind {
                                if long.iter().any(|o| o == long_option) {
                                    if let Some(value) = value {
                                        parsed_command.arguments.insert(argument_id, Value::String(value.to_string()));
                                    } else {
                                        context = Context::OptionValue(argument_id);
                                    }
                                    found = true;
                                }
                            }
                        }

                        if !found {
                            return Err(Error::UnexpectedArgument);
                        }
                    } else if let Some(short_options) = argument.strip_prefix('-') {
                        for short_option in short_options.chars() {
                            let mut found = false;
                            for (argument_id, command_argument) in &command.arguments {
                                if let ArgumentKind::Option { short, .. } = &command_argument.kind {
                                    if short.contains(&short_option) {
                                        context = Context::OptionValue(argument_id);
                                        found = true;
                                        break;
                                    }
                                }
                            }

                            if !found {
                                return Err(Error::UnexpectedArgument);
                            }
                        }
                    } else {
                        let mut found = false;
                        for (argument_id, command_argument) in &command.arguments {
                            if let ArgumentKind::Positional { position } = &command_argument.kind {
                                if *position == current_position {
                                    parsed_command.arguments.insert(argument_id, Value::String(argument.to_string()));
                                    current_position += 1;
                                    found = true;
                                    break;
                                }
                            }
                        }

                        if !found {
                            return Err(Error::UnexpectedArgument);
                        }
                    }
                },
                Context::OptionValue(argument_id) => {
                    parsed_command.arguments.insert(argument_id, Value::String(argument.to_string()));
                },
            }
        }
    }

    Ok(parsed_command)
}

pub fn split_arguments(command: &str) -> Result<Vec<String>, Error> {
    enum Context {
        Default,
        SingleQuotes,
        DoubleQuotes,
    }

    let mut context = Context::Default;
    let mut arguments = Vec::new();
    let mut argument = String::new();
    let mut chars_iter = command.chars().peekable();
    while let Some(char) = chars_iter.next() {
        let parsed_char = match char {
            '"' => match context {
                Context::Default => {
                    context = Context::DoubleQuotes;
                    None
                },
                Context::DoubleQuotes => {
                    context = Context::Default;
                    None
                },
                Context::SingleQuotes => Some(char),
            },
            '\'' => match context {
                Context::Default => {
                    context = Context::SingleQuotes;
                    None
                },
                Context::SingleQuotes => {
                    context = Context::Default;
                    None
                },
                Context::DoubleQuotes => Some(char),
            },
            '\\' => match chars_iter.peek() {
                Some(c) if *c == '"' => match context {
                    Context::Default | Context::DoubleQuotes => Some(*c),
                    _ => None,
                },
                Some(c) if ['\'', '\\', ' '].contains(c) => match context {
                    Context::Default => Some(*c),
                    _ => None,
                },
                Some('n') => Some('\n'),
                Some(_) => None,
                None => return Err(Error::UnmatchedBackslash),
            }.or(Some(char)),
            ' ' => match context {
                Context::Default => {
                    arguments.push(argument);
                    argument = String::new();
                    None
                },
                Context::DoubleQuotes | Context::SingleQuotes => Some(char),
            },
            _ => Some(char),
        };

        if let Some(parsed_char) = parsed_char {
            argument.push(parsed_char);
        }
    }
    arguments.push(argument);

    Ok(arguments)
}

#[derive(Debug)]
pub enum Error {
    UnexpectedArgument,
    UnknownCommand,
    UnmatchedQuotes,
    UnmatchedBackslash,
}

pub type CommandTree = HashMap<CommandId, Command>;

pub struct Command {
    pub arguments: HashMap<String, Argument>,
    pub commands: CommandTree,
}

pub struct Argument {
    pub kind: ArgumentKind,
    pub value: Option<ValueKind>,
    pub optional: bool,
}

pub enum ArgumentKind {
    Positional {
        position: u8,
    },
    Option {
        short: Vec<char>,
        long: Vec<String>,
    }
}

#[derive(PartialEq, Eq, Hash)]
pub enum CommandId {
    Default,
    Name(String),
}

impl CommandId {
    fn as_ref(&self) -> CommandIdRef {
        match self {
            Self::Default => CommandIdRef::Default,
            Self::Name(name) => CommandIdRef::Name(name),
        }
    }
}

impl PartialEq<CommandIdRef<'_>> for CommandId {
    fn eq(&self, other: &CommandIdRef) -> bool {
        match (self, other) {
            (Self::Default, CommandIdRef::Default) => true,
            (Self::Name(self_name), CommandIdRef::Name(other_name)) => self_name == other_name,
            _ => false,
        }
    }
}

#[derive(Debug)]
pub enum CommandIdRef<'a> {
    Default,
    Name(&'a str),
}

#[derive(Debug)]
pub struct ParsedCommand<'a> {
    pub command: Vec<CommandIdRef<'a>>,
    pub arguments: HashMap<&'a str, Value>,
}

pub enum ValueKind {
    String,
}

#[derive(Debug)]
pub enum Value {
    String(String),
}

impl Value {
    pub fn try_as_str(&self) -> Result<&str, ()> {
        match self {
            Self::String(v) => Ok(&v),
            // _ => Err(()),
        }
    }
}
